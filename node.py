import json
import logging
import random
import socket
import threading
from datetime import datetime
from time import sleep

import psutil

from import_header import *
from membership_list import MembershipList


class Node:
    def __init__(self,
                 introducer_domain: str,
                 use_gossip: bool,
                 domain: str):
        self.domain = domain
        self.ip = socket.gethostbyname(domain)
        self.introducer_domain = introducer_domain
        self.introducer_ip = socket.gethostbyname(introducer_domain)
        self.use_gossip = use_gossip

        self.local_ip = socket.gethostbyname(socket.gethostname())
        self.local_port = DEFAULT_PORT
        self.control_port = CONTROL_PORT
        now = datetime.timestamp(datetime.now())
        self.id = str(self.local_ip) + ":" + str(self.local_port) + ":" + str(now)  # ip:port:ts
        self.msl = MembershipList(self.id, self.ip, now, self.local_ip)
        self.last_pinged_by = None
        self.current_bandwidth = -1

    # threads func

    def receiver(self):
        """
        Listen to incoming messages
        """
        logger = logging.getLogger('receiver')
        logger.warning("receiver is on")
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.bind((self.local_ip, self.local_port))
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # prevent 'address already in use'
            while True:
                try:
                    logger.debug("waiting for length of 8 bytes")
                    # check msg size
                    data, _ = s.recvfrom(8)
                    msg_size = int(data.decode())

                    # do nothing if self.status == LEAVE
                    if self.msl.get_status(self.local_ip) == Status.LEAVE:
                        continue

                    logger.debug(f"waiting for length of {msg_size} bytes")
                    # receive actual message
                    data, _ = s.recvfrom(msg_size)
                    data = data.decode()
                    msg = json.loads(data)
                    logger.debug(f"got json: {data}")

                    # handle PING
                    if msg[MessageContent.TYPE] == MessageType.PING:
                        self.last_pinged_by = msg[MessageContent.FROM_IP]
                        
                        # send membership list for merging on the target node
                        if self.use_gossip:
                            logger.debug(f"Gossip-style merge with {self.last_pinged_by}")
                            self.msl.merge(msg[MessageContent.CONTENT])
                        else:
                            logger.debug(f"All-to-all merge with {self.last_pinged_by}")
                            self.msl.merge(msg[MessageContent.CONTENT])

                    # handle JOIN_ACK
                    elif msg[MessageContent.TYPE] == MessageType.JOIN_ACK:
                        self.last_pinged_by = msg[MessageContent.FROM_IP]

                        if msg[MessageContent.CURR_HB_MODE] == HeartbeatModeType.GOSSIP:
                            self.use_gossip = True
                        else:
                            self.use_gossip = False

                        if self.use_gossip:
                            self.msl.merge(msg[MessageContent.CONTENT])
                        else:
                            self.msl.merge(msg[MessageContent.CONTENT])
                        
                        self.msl.set_status(self.local_ip, Status.JOINED)
                except ValueError:
                    pass
                except Exception as e:
                    print(e)

    def pinger(self):
        """
        Ping other nodes periodically and update membership list and heartbeat table
        """
        logger = logging.getLogger("pinger")
        while True:
            sleep(T_HB_PERIOD)
            # logger.debug("iter start")

            # do nothing if self.status == LEAVE
            if self.msl.get_status(self.local_ip) == Status.LEAVE:
                continue

            # increase heartbeat of node itself
            self.msl.increase_heartbeat(self.local_ip)
            # logger.debug(f"increase self heartbeat to {self.msl.get_heartbeat(self.local_ip)}")

            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
                # if using gossip-style
                if self.use_gossip:
                    # randomly choose multiple(GOSSIP_NUM) target nodes
                    possible_targets = [k for k in self.msl.get_keys()
                                        if k != self.local_ip and k != self.last_pinged_by]
                    if int(datetime.timestamp(datetime.now())) % 3 == 0:
                        logger.info(("gossip" if self.use_gossip else "all-to-all")
                                    + f" | possible targets: {possible_targets}")

                    failed_nodes = [x for x in self.msl.get_keys() 
                                    if self.msl.get_status(x) == Status.FAILURE]

                    # if there are less than set possible targets, send to all
                    # if there are more possible targets than set, use choices()
                    for each_target in possible_targets if len(possible_targets) <= GOSSIP_NUM \
                            else random.choices(possible_targets, k=GOSSIP_NUM):
                        
                        if self.msl.get_status(each_target) == Status.FAILURE:
                            continue

                        now = datetime.timestamp(datetime.now())
                        # send PING 
                        msg = {
                            MessageContent.TYPE: MessageType.PING,
                            MessageContent.FROM_IP: self.local_ip,
                            MessageContent.TIMESTAMP: now,
                            MessageContent.CONTENT: self.msl.get_list_except(failed_nodes) # ignore failed nodes
                        }

                        msg_str = json.dumps(msg)
                        logger.debug(f"Sending msg to {each_target}: {msg_str}")

                        self.send_msg_str(each_target, DEFAULT_PORT, msg_str, loss_rate=LOSS_RATE)
                # if using all-to-all
                else:
                    # take every node except self as target
                    target_list = [k for k in self.msl.get_keys() if k != self.local_ip]
                    if int(datetime.timestamp(datetime.now())) % 3 == 0:
                        logger.info(("gossip" if self.use_gossip else "all-to-all")
                                    + f" | target list: {target_list}")

                    for each_target in target_list:
                        if self.msl.get_status(each_target) == Status.FAILURE:
                            continue
                        now = datetime.timestamp(datetime.now())
                        # send PING 
                        msg = {
                            MessageContent.TYPE: MessageType.PING,
                            MessageContent.FROM_IP: self.local_ip,
                            MessageContent.TIMESTAMP: now,
                            MessageContent.CONTENT: self.msl.get_complete_list()
                        }

                        msg_str = json.dumps(msg)
                        logger.debug(f"Sending msg to {each_target}: {msg_str}")

                        self.send_msg_str(each_target, DEFAULT_PORT, msg_str, loss_rate=LOSS_RATE)

    def membership_checker(self):
        """
        Periodically check membership list for timeouts
        """
        while True:
            if self.msl.get_status(self.local_ip) == Status.LEAVE:
                sleep(1)
                continue
            to_delete = []
            for ip in self.msl.get_keys():
                now = datetime.now()
                if ip != self.local_ip:
                    # if using gossip-style
                    if self.use_gossip:
                        time_last = datetime.fromtimestamp(self.msl.get_timestamp(ip))
                        time_delta = (now - time_last).seconds
                        status = self.msl.get_status(ip)
                        if status != Status.FAILURE:
                            # if node heartbeat not changed after T_fail, mark the node as failed
                            if time_delta >= T_FAIL:
                                self.msl.set_status(ip, Status.FAILURE)
                        elif status == Status.FAILURE:
                            time_delta -= T_FAIL
                            # if node status not changed after marked failed in T_cleanup, delete entry
                            if time_delta >= T_CLEANUP:
                                to_delete.append(ip)
                    # if using all-to-all
                    else:
                        if self.msl.get_status(ip) != Status.FAILURE:
                            time_last = datetime.fromtimestamp(self.msl.get_timestamp(ip))
                            time_delta = now - time_last
                            # if heartbeat not changed during the entire heartbeat period, mark the node as failed
                            if time_delta.seconds > float(T_HB_PERIOD):
                                self.msl.set_status(ip, Status.FAILURE)
            for x in to_delete:
                self.msl.delete_entry(x)
            sleep(T_HB_PERIOD)

    def cli_receiver(self):
        """
        Listen to incoming messages initiated by cli commands
        """
        logger = logging.getLogger('cli_receiver')
        logger.warning("cli_receiver is on")
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.bind((self.local_ip, self.control_port))
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # prevent 'address already in use'
            while True:
                try:
                    logger.debug("waiting for length of 8 bytes")
                    # check msg size
                    data, _ = s.recvfrom(8)
                    msg_size = int(data.decode())

                    # do nothing if self.status == LEAVE
                    if self.msl.get_status(self.local_ip) == Status.LEAVE:
                        continue

                    logger.debug(f"waiting for length of msg_size={msg_size} bytes")
                    # receive actual message
                    data, _ = s.recvfrom(msg_size)
                    msg = json.loads(data.decode())

                    # handle LEAVE
                    if msg[MessageContent.TYPE] == MessageType.LEAVE:
                        logger.debug("TODO")
                        self.msl.delete_entry(msg[MessageContent.FROM_IP])
                    # handle JOIN_REQUEST
                    elif msg[MessageContent.TYPE] == MessageType.JOIN_REQUEST:
                        if self._is_introducer():
                            logger.debug("TODO")
                            # merge the new node into membership list
                            self.msl.merge(msg[MessageContent.CONTENT])

                            # reply with JOIN_ACK
                            reply_msg = {
                                MessageContent.TYPE: MessageType.JOIN_ACK,
                                MessageContent.FROM_IP: self.local_ip,
                                MessageContent.CONTENT: self.msl.get_complete_list(),
                                MessageContent.CURR_HB_MODE: HeartbeatModeType.GOSSIP 
                                    if self.use_gossip else HeartbeatModeType.ALLTOALL
                            }

                            reply_msg_str = json.dumps(reply_msg)

                            # with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
                            self.send_msg_str(msg[MessageContent.FROM_IP], DEFAULT_PORT, reply_msg_str)
                    # handle MODE_CHANGE
                    elif msg[MessageContent.TYPE] == MessageType.MODE_CHANGE:
                        self.use_gossip = msg[MessageContent.CONTENT] == HeartbeatModeType.GOSSIP
                except Exception as e:
                    print(e)

    def cli(self):
        """
        Read command inputs to carry out actions
        """
        logger = logging.getLogger('cli')
        while True:
            raw_input = input("425 Node $ " if SHOW_PROMPT else "")
            try:
                cmd = raw_input.split()[0].lower()
                # args = raw_input.split()[1:]
            except IndexError:
                continue

            # switch mode
            if "switch".startswith(cmd):
                # default header for mode change message
                msg = {
                    MessageContent.TYPE: MessageType.MODE_CHANGE,
                    MessageContent.FROM_IP: self.local_ip,
                }

                # if already in gossip mode, change to all-to-all
                if self.use_gossip:
                    self.use_gossip = False
                    msg[MessageContent.CONTENT] = HeartbeatModeType.ALLTOALL
                # if in all-to-all mode, change to gossip
                else:
                    self.use_gossip = True
                    msg[MessageContent.CONTENT] = HeartbeatModeType.GOSSIP
                
                msg_str = json.dumps(msg)
                    
                # tell every other nodes about the change
                for each_ip in [each_ip for each_ip in self.msl.get_keys() if each_ip != self.local_ip]:
                    self.send_msg_str(each_ip, CONTROL_PORT, msg_str)

                        
            # list membership list
            elif "ml".startswith(cmd) or "membershiplist".startswith(cmd):
                print(self.msl)
            # list self id
            elif "id".startswith(cmd):
                print(self.id)
            # join the group
            elif "join".startswith(cmd):
                # self-introduce
                self.msl.set_status(self.local_ip, Status.JOINED)
                if not self._is_introducer():
                    # create a new up-to-date id for node itself
                    now = datetime.timestamp(datetime.now())
                    self.id = str(self.local_ip) + ":" + str(self.local_port) + ":" + str(now)  # ip:port:ts
                    self.msl.set_id(self.local_ip, self.id)
                    self.msl.set_timestamp(self.local_ip, now)

                    # compose JOIN message
                    msg = {
                        MessageContent.TYPE: MessageType.JOIN_REQUEST,
                        MessageContent.FROM_IP: self.local_ip,
                        MessageContent.CONTENT: {self.local_ip: self.msl.get_value(self.local_ip)}
                    }
                    msg_str = json.dumps(msg)
                    logger.debug(f"Sending msg to {self.introducer_ip}: {str(len(msg_str)).zfill(8)}")
                    logger.debug(f"Sending msg to {self.introducer_ip}: {msg_str}")

                    # use UDP to send JOIN message to the introducer
                    self.send_msg_str(self.introducer_ip, CONTROL_PORT, msg_str)
                else:
                    # refresh timestamp for every entry, so they don't timeout immediately
                    for each_ip in self.msl.get_keys():
                        self.msl.set_timestamp(each_ip, datetime.timestamp(datetime.now()))

                    self.msl.set_status(self.local_ip, Status.JOINED)
            # voluntarily leave the group
            elif "leave".startswith(cmd):
                # compose LEAVE message
                msg = {
                    MessageContent.TYPE: MessageType.LEAVE,
                    MessageContent.FROM_IP: self.local_ip
                }
                msg_str = json.dumps(msg)
                logger.debug(f"Sending msg to (#TODO): {msg_str}")

                for each_ip in [each_ip for each_ip in self.msl.get_keys() if each_ip != self.local_ip]:
                    self.send_msg_str(each_ip, CONTROL_PORT, msg_str)

                if not self._is_introducer(): # introducer should keep the membership list for later re-joins
                    # clear membership list
                    self.msl.clear_other_nodes_except(self.local_ip)

                # change self status
                self.msl.set_status(self.local_ip, Status.LEAVE)

            # not defined
            else:
                pass

    def bandwidth_checker(self):
        prev = 0
        f = open("bandwidth.csv", "w", buffering=1)
        while True:
            curr = psutil.net_io_counters().bytes_sent + psutil.net_io_counters().bytes_recv
            if prev:
                delta = curr - prev
                delta = delta/1024./1024.*8  # to Mbit
                self.current_bandwidth = delta
                f.write(f"{int(datetime.timestamp(datetime.now()))},{delta}\n")
            prev = curr
            sleep(T_HB_PERIOD)
    
    def false_positive_rate_checker(self):
        f = open("false_positive_rate.csv", "w", buffering=1)
        while True:
            s = len([x for x in self.msl.get_keys() if self.msl.get_status(x) == Status.FAILURE])
            f.write(f"{int(datetime.timestamp(datetime.now()))},{s}\n")
            sleep(T_HB_PERIOD)

    # ---Helper Functions Below---

    def _is_introducer(self):
        logging.getLogger("_is_introducer")\
            .debug(f"self.domain={self.domain}; self.introducer_domain={self.introducer_domain}")
        return self.domain == self.introducer_domain
    
    def send_msg_str(self, dest_ip, dest_port, msg_str, loss_rate=0):
        if loss_rate <= random.random():
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
                s.sendto(str(len(msg_str)).zfill(8).encode(), (dest_ip, dest_port))
                s.sendto(msg_str.encode(), (dest_ip, dest_port))
        else:
            pass

    def run(self):
        thread_receiver = threading.Thread(target=self.receiver)
        thread_pinger = threading.Thread(target=self.pinger)
        thread_cli = threading.Thread(target=self.cli)
        thread_cli_receiver = threading.Thread(target=self.cli_receiver)
        thread_membership_checker = threading.Thread(target=self.membership_checker)
        thread_bandwidth = threading.Thread(target=self.bandwidth_checker)
        thread_false_positive_rate = threading.Thread(target=self.false_positive_rate_checker)

        thread_receiver.start()
        thread_pinger.start()
        thread_cli.start()
        thread_cli_receiver.start()
        thread_membership_checker.start()
        thread_bandwidth.start()
        thread_false_positive_rate.start()

        thread_receiver.join()
        thread_pinger.join()
        thread_cli.join()
        thread_cli_receiver.join()
        thread_membership_checker.join()
        thread_bandwidth.join()
        thread_false_positive_rate.join()


def main():
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s [%(levelname)s] %(name)s: %(message)s')
    logging.getLogger("main").warning(f"Use gossip: {INIT_USE_GOSSIP}")
    server = Node(introducer_domain=INTRODUCER, use_gossip=INIT_USE_GOSSIP, domain=socket.gethostname())
    server.run()


if __name__ == '__main__':
    main()
