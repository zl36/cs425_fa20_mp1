if [[ -z "$1" ]]; then
  echo "please select node"
else
  chmod 600 id_rsa
  ct=1
  for i in $(cat server_list.txt); do
    if [[ "$1" == "$ct" ]]; then
      ssh -i id_rsa zl36@"$i"
    fi
    ct+=1
  done
fi