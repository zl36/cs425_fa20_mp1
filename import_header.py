# General Constants
DEFAULT_PORT = 12345
CONTROL_PORT = 12346

INTRODUCER = 'fa20-cs425-g30-01.cs.illinois.edu'

VM_SERVERS = [
    'fa20-cs425-g30-01.cs.illinois.edu',
    'fa20-cs425-g30-02.cs.illinois.edu',
    'fa20-cs425-g30-03.cs.illinois.edu',
    'fa20-cs425-g30-04.cs.illinois.edu',
    'fa20-cs425-g30-05.cs.illinois.edu',
    'fa20-cs425-g30-06.cs.illinois.edu',
    'fa20-cs425-g30-07.cs.illinois.edu',
    'fa20-cs425-g30-08.cs.illinois.edu',
    'fa20-cs425-g30-09.cs.illinois.edu',
    'fa20-cs425-g30-10.cs.illinois.edu'
]

SHOW_PROMPT = True
LOSS_RATE = 0  # set message loss rate
INIT_USE_GOSSIP = True

# gossip constants
T_FAIL = 3
T_CLEANUP = 7
T_HB_PERIOD = 1
GOSSIP_NUM = 3

# Enumerators
class Status:
    FAILURE = 'FAILURE'
    JOINED = 'JOINED'
    LEAVE = 'LEAVE'


# define message body
# eg. msg = {
#   ID: "id",
#   TYPE: MessageType,
#   FROM_IP: "ip",
#   TIMESTAMP: "ts",
#   CONTENT: anything
# }

class HeartbeatModeType:
    GOSSIP = 'GOSSIP'
    ALLTOALL = 'ALL_TO_ALL'

class MessageType:
    JOIN_REQUEST = 'JOIN_REQUEST'
    JOIN_ACK = 'JOIN_ACK'
    PING = 'PING'
    LEAVE = 'LEAVE'
    MODE_CHANGE = 'MODE_CHANGE'


class MessageContent:
    ID = 'ID'
    TYPE = 'TYPE'
    FROM_IP = 'FROM_IP'
    TIMESTAMP = 'TIMESTAMP'
    CONTENT = 'CONTENT'
    CURR_HB_MODE = 'CURR_HB_MODE'
