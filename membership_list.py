from datetime import datetime
from threading import Lock

from import_header import *

import logging


class MembershipList:
    def __init__(self, set_id, set_name, set_ts, ip):
        self.lock = Lock()
        self.dict = {
            ip: {
                'name': set_name,
                'status': Status.LEAVE,
                'hb': 0,
                'ts': set_ts,
                'id': set_id
            }
        }
        self.local_ip = ip
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s [%(levelname)s] %(name)s: %(message)s')
        self.logger = logging.getLogger("MembershipList")

    def __len__(self):
        return len(self.dict)

    def __getitem__(self, item):
        return self.dict[item]

    def __str__(self):
        ret = f"Lock={self.lock.locked()}\nLength={len(self.dict.items())}\n"
        for k, v in self.dict.items():
            ret += 'IP:%s Status:%s HeartBeat:%d TimeStamp:%f' % (v['name'], v['status'], v['hb'], v['ts'])
            ret += f"; delta={(datetime.now() - datetime.fromtimestamp(v['ts'])).seconds}\n"
        return ret

    def new_entry(self, new_id, name, status, heartbeat, timestamp, ip):
        self.logger.info(f"A new entry ({ip}) is added to {self.local_ip}")
        self.lock.acquire()
        self.dict[ip] = {
            'name': name,
            'status': status,
            'hb': heartbeat,
            'ts': timestamp,
            'id': new_id
        }
        self.lock.release()

    def delete_entry(self, id_to_delete):
        self.logger.info(f"An entry ({id_to_delete}) is removed from {self.local_ip}")
        self.lock.acquire()
        del self.dict[id_to_delete]
        self.lock.release()

    def increase_heartbeat(self, ip):
        hb = self.get_heartbeat(ip)
        self.set_heartbeat(ip, hb + 1)

    def merge(self, merge_with):
        # self.logger.info(f"This ({self.local_ip}) is merged with {merge_with}")
        self.lock.acquire()
        now = datetime.timestamp(datetime.now())
        for each_ip in merge_with.keys():
            # if a node is not already present, add to membership list
            if each_ip not in self.dict:
                self.dict[each_ip] = merge_with[each_ip]
                self.dict[each_ip]['ts'] = now
            else:
                # if the id in merge_list indicate a newer join session, replace entirely
                merge_id_time = float(merge_with[each_ip]['id'].split(':')[2])
                this_id_time = float(self.get_timestamp(each_ip))
                if merge_id_time > this_id_time:
                    self.dict[each_ip] = merge_with[each_ip]
                    self.dict[each_ip]['ts'] = now
                # update heartbeat to the higher value of the two
                if self.dict[each_ip]['hb'] < merge_with[each_ip]['hb']:
                    self.lock.release()
                    self.set_heartbeat(each_ip, merge_with[each_ip]['hb'])
                    self.set_timestamp(each_ip, now)
                    self.lock.acquire()
                # self.lock.release()
                # self.set_status(each_ip, Status.JOINED)
                # self.lock.acquire()
        self.lock.release()
    
    def clear_other_nodes_except(self, except_ip):
        self.logger.info(f"This ({self.local_ip}) cleared all other memberships!")
        for each_ip in [x for x in self.dict.keys() if x != except_ip]:
            self.delete_entry(each_ip)

    # getter and setter
    def get_keys(self):
        return self.dict.keys()
    
    def get_value(self, ip):
        return self.dict[ip]

    def get_name(self, ip):
        return self.dict[ip]['name']

    def get_heartbeat(self, ip):
        return self.dict[ip]['hb']
    
    def get_status(self, ip):
        return self.dict[ip]['status']

    def get_timestamp(self, ip):
        return self.dict[ip]['ts']

    def get_id(self, ip):
        return self.dict[ip]['id']

    def get_complete_list(self):
        return self.dict
    
    def get_list_except(self, except_list):
        new_dict = {}
        for each_ip in [k for k in self.dict.keys() if k not in except_list]:
            new_dict[each_ip] = self.get_value(each_ip)
        return new_dict

    def set_name(self, ip, name):
        self.lock.acquire()
        self.dict[ip]['name'] = name
        self.lock.release()

    def set_heartbeat(self, ip, new_hb):
        self.lock.acquire()
        self.dict[ip]['hb'] = new_hb
        self.lock.release()
        self.set_timestamp(ip, datetime.timestamp(datetime.now()))

    def set_status(self, ip, new_status):
        self.logger.info(f"{self.local_ip} -> {ip} changed to: {new_status}")
        self.lock.acquire()
        self.dict[ip]['status'] = new_status
        self.lock.release()

    def set_timestamp(self, ip, new_ts):
        self.lock.acquire()
        self.dict[ip]['ts'] = new_ts
        self.lock.release()
    
    def set_id(self, ip, new_id):
        self.lock.acquire()
        self.dict[ip]['id'] = new_id
        self.lock.release()
