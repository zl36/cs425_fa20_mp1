# cs425_fa20_mp1

Usage:

```
python3 ./node.py
```

There will be a cli shell. You can input "id", "switch", "ml" (membership list), "join", and "leave"

To exit, press Ctrl-C twice.